import { Schema } from 'mongoose'
import { createService } from '@/lib'

const UserSchema = new Schema(
  {
    email: {
      type: String,
      required: true
    },
    firstName: {
      type: String,
      required: true
    },
    lastName: {
      type: String,
      required: true
    }
  },
  { collection: 'users' }
)

UserSchema.set('timestamps', true)

export const userService = createService('User', UserSchema)
