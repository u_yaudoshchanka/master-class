import mongoose from 'mongoose'

import { DATABASE } from '@/constants'
import { url } from '@/constants/database'

export const initDB = () => {
  mongoose.connect(url, DATABASE)
  mongoose.connection.on('error', err => {
    console.log('error', err)
  })
}
