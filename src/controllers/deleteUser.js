import { STATUS_CODES } from '@/constants'
import { userService } from '@/schemas'

const { SERVER_ERROR, NOT_FOUND, OK } = STATUS_CODES

export const deleteUser = async (req, res) => {
  try {
    const { id } = req.params

    const user = await userService.getById(id)

    if (!user) {
      return res.status(NOT_FOUND).send({
        err: 'User not found'
      })
    }

    await userService.delete({
      _id: id
    })
    return res.status(OK).send('User has been deleted')
  } catch (err) {
    return res.status(SERVER_ERROR).send({
      err
    })
  }
}
