import { STATUS_CODES } from '@/constants'
import { userService } from '@/schemas'

const { SERVER_ERROR, OK } = STATUS_CODES

export const getUsers = async (req, res) => {
  try {
    const users = await userService.getMany()
    return res.status(OK).send({
      users
    })
  } catch (err) {
    return res.status(SERVER_ERROR).send({
      err
    })
  }
}
