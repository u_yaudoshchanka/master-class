import { STATUS_CODES } from '@/constants'
import { userService } from '@/schemas'

const { SERVER_ERROR, OK } = STATUS_CODES

export const createUser = async (req, res) => {
  try {
    const { ...fields } = req.body

    const user = await userService.create({ ...fields })

    return res.status(OK).send({
      user
    })
  } catch (err) {
    return res.status(SERVER_ERROR).send({
      err
    })
  }
}
