import { STATUS_CODES } from '@/constants'
import { userService } from '@/schemas'

const { SERVER_ERROR, NOT_FOUND, OK } = STATUS_CODES

export const updateUser = async (req, res) => {
  try {
    const { id } = req.params
    const { ...fields } = req.body

    const user = await userService.getById(id)

    if (!user) {
      return res.status(NOT_FOUND).send({
        err: 'User not found'
      })
    }

    await userService.update(
      { _id: id },
      { ...fields }
    )

    return res.status(OK).send('User has been updated')
  } catch (err) {
    return res.status(SERVER_ERROR).send({
      err
    })
  }
}
