import express from 'express'
import bodyParser from 'body-parser'
import {
  initDB
} from '@/lib'
import apiRouter from '@/routes/api'
import config from '@/config'

try {
  const app = express()
  app.use(bodyParser.json())
  app.use('/api', apiRouter)
  const PORT = config.PORT || 3030

  initDB()

  app.listen(PORT, () => {
    console.log(`you are server is running on ${PORT}`)
  })
} catch (err) {
  console.log('error: ', err)
}
