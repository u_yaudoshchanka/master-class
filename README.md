### Prerequisites

- Node.js v10.x.x
- MongoDB
- YARN is preferred, but NPM is possible

### ENV Vars

- **PORT** - set the port to run the application
- **MONGO_DB_URL** - set the url of running MongoDB instance
- **MONGO_DB_USER** - set the username of running MongoDB instance
- **MONGO_DB_PASSWORD** - set the password of running MongoDB instance

### How to run

- `git clone` _repository-url_
- `cd` _./folder-name_
- `yarn install`
- create your own `.env` file (you can find example at `./env.example`)
- `yarn start` or `yarn dev` for watching

## **Be sure that MongoDB daemon is launched**
